
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) //Checks if the delay seconds are more then 0
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); //If they are less than zero subtract the delay seconds  

		if (m_delaySeconds <= 0)//Check if the delay seconds are less than or equal to 0
		{
			GameObject::Activate(); //If the game objects are less than or equal to 0 activate the enemy ship
		}
	}

	if (IsActive()) //Checks if the ship is active
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); //Add the time activated in seconds
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); //Checks if the time activated is greater than 2 and if the enemy ship is on the screen 
																	//If it is on the screen Deactivate it
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}