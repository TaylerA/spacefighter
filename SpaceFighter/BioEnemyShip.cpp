
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive()) //Checks if the bio enemy ship is active
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); //Makes the enemies move around using the sin function
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f; //Sets the speed for the bio enemy ship
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());//Moves the position by getting the speed and the time elapsed

		if (!IsOnScreen()) Deactivate(); //If the object isn't on the screen deactivate it
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
